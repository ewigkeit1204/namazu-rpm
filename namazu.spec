#%%define _use_internal_dependency_generator 0
%define	nmz_localstatedir	%{_localstatedir}/lib
%define	nmz_libexecdir		%{_localstatedir}/www/cgi-bin

Name:		namazu
Version:	2.0.22pre8
Release:	4%{?dist}
License:	GPLv2+
URL:		http://www.namazu.org/
BuildRequires:	perl >= 5.26.0, perl(NKF) >= 2.07, perl(Text::Kakasi) >= 1.00
BuildRequires:	perl(File::MMagic) >= 1.12
BuildRequires:	nkf kakasi mecab
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0:		http://www.namazu.org/test/%{name}-%{version}.tar.gz
Source1:    filter-requires-namazu.sh

Patch1:		namazu-newgettext3.patch
Patch3:		namazu-multilib.patch

%define __perl_requires %{SOURCE1}
%define __perllib_requires %{SOURCE1}

Summary:	A full-text search engine
Requires:	perl >= 5.26.0, perl(File::MMagic) >= 1.12, perl(NKF) >= 2.07
Requires:	kakasi >= 2.3.0, perl(Text::Kakasi) >= 1.00
Group:		Applications/Text

%description
Namazu is a full-text search engine software intended for easy use.
Not only it works as CGI program for small or medium scale WWW
search engine, but also works as personal use such as search system
for local HDD.

%package libs
Summary:	Libraries for Namazu
Group:		Development/Libraries
Obsoletes:	%{name} < 2.0.17-3%{?dist}

%description libs
Namazu is a full-text search engine software intended for easy use.

This package contains the namazu library.

%package devel
Summary:	Libraries and include files of Namazu
Group:		Development/Libraries
Requires:	%{name}-libs = %{version}-%{release}

%description devel
Libraries and include files of Namazu.


%package cgi
Summary:	A CGI interface for Namazu
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}
Requires:	webserver

%description cgi
A CGI interface for Namazu.


%prep 
%setup -q
%patch1 -p1 -b .newgettext
%patch3 -p1 -b .multilib

%build
%configure --localstatedir=%{nmz_localstatedir} --libexecdir=%{nmz_libexecdir} --disable-static
make %{?_smp_flags}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p"

# correct email address
sed -i -e "s/\(\$ADDRESS = 'webmaster@\).*\(';\)/\1localhost\2/" $RPM_BUILD_ROOT%{_datadir}/namazu/pl/conf.pl

# correct timestamp
touch -r nmz-config.in.multilib $RPM_BUILD_ROOT%{_bindir}/nmz-config
touch -r pl/conf.pl.in $RPM_BUILD_ROOT%{_datadir}/namazu/pl/conf.pl

# namazurc
mv $RPM_BUILD_ROOT%{_sysconfdir}/namazu/namazurc-sample \
	$RPM_BUILD_ROOT%{_sysconfdir}/namazu/namazurc
# mknmzrc
sed -e "s/\(\$ADDRESS = 'webmaster@\).*\(';\)/\1\2/" $RPM_BUILD_ROOT%{_sysconfdir}/namazu/mknmzrc-sample > $RPM_BUILD_ROOT%{_sysconfdir}/namazu/mknmzrc
touch -r $RPM_BUILD_ROOT%{_sysconfdir}/namazu/mknmzrc-sample $RPM_BUILD_ROOT%{_sysconfdir}/namazu/mknmzrc
rm $RPM_BUILD_ROOT%{_sysconfdir}/namazu/mknmzrc-sample

chmod 755 -R $RPM_BUILD_ROOT%{nmz_localstatedir}/namazu
chmod 755 -R $RPM_BUILD_ROOT%{nmz_localstatedir}/namazu/index

# don't ship ja.po for sjis.
rm -rf $RPM_BUILD_ROOT%{_datadir}/locale/ja_JP.SJIS
rm -rf $RPM_BUILD_ROOT%{_datadir}/namazu/etc
rm -rf $RPM_BUILD_ROOT%{_datadir}/namazu/doc
rm -rf $RPM_BUILD_DIR/%{name}-%{version}/doc/en/Makefile*
rm -rf $RPM_BUILD_DIR/%{name}-%{version}/doc/ja/Makefile*

rm $RPM_BUILD_ROOT%{_libdir}/*.la
rm -rf $RPM_BUILD_ROOT%{_datadir}/locale/ja_JP.SJIS

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-, root, root, -)
%doc AUTHORS ChangeLog ChangeLog.1 CREDITS COPYING
%doc README NEWS THANKS TODO
%doc etc/namazu.png doc/en doc/namazu.css
%lang(es) %doc README-es
%lang(ja) %doc README-ja doc/ja
%dir %{_sysconfdir}/namazu
%dir %{_datadir}/namazu
%dir %{_datadir}/namazu/filter
%dir %{_datadir}/namazu/pl
%dir %{_datadir}/namazu/template
%config(noreplace) %{_sysconfdir}/namazu/*
%{_bindir}/namazu
%{_bindir}/bnamazu
%{_bindir}/*nmz
%{_bindir}/mailutime
%{_bindir}/nmzgrep
%{_bindir}/nmzmerge
%{_bindir}/nmzcat
%{_bindir}/nmzegrep
%{_mandir}/man1/*
%{_datadir}/namazu/filter/*
%{_datadir}/namazu/pl/*
%{_datadir}/namazu/template/*
%dir %{nmz_localstatedir}/namazu
%dir %{nmz_localstatedir}/namazu/index

%files libs
%defattr(-, root, root, -)
%doc CREDITS COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root, -)
%doc CREDITS COPYING HACKING
%lang(ja) %doc HACKING-ja
%dir %{_includedir}/namazu
%{_bindir}/nmz-config
%{_includedir}/namazu/*.h
%{_libdir}/*.so

%files cgi
%defattr(-, root, root, -)
%doc CREDITS COPYING
%{nmz_libexecdir}/namazu.cgi


%changelog
* Fri Jan 22 2021 Keisuke Kamada <ewigkeit1204@gmail.com> - 2.0.22pre8-4
- Move namazu.cgi to /var/www/cgi-bin

* Thu Jan 21 2021 Keisuke Kamada <ewigkeit1204@gmail.com> - 2.0.22pre8-3
- Add dependency of kakasi

* Wed Jan 20 2021 Keisuke Kamada <ewigkeit1204@gmail.com> - 2.0.22pre8-2
- Use perl-mecab.

* Fri Nov 13 2020 Keisuke Kamada <ewigkeit1204@gmail.com> - 2.0.22pre8-1
- New upstream release.
- Remove kakasi from dendencies.

* Thu Apr 23 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 2.0.22pre6-1
- New upstream release.

* Mon Feb 14 2011 Akira TAGOH <tagoh@redhat.com> - 2.0.19-5
- Fix a broken deps issue.

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.19-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.19-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Mar 30 2009 Akira TAGOH <tagoh@redhat.com> - 2.0.19-2
- Fix a broken deps.

* Wed Mar 18 2009 Akira TAGOH <tagoh@redhat.com> - 2.0.19-1
- New upstream release.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.18-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Akira TAGOH <tagoh@redhat.com> - 2.0.18-7
- simplify a package summary.

* Wed Sep  3 2008 Akira TAGOH <tagoh@redhat.com> - 2.0.18-6
- Fix a broken deps.

* Mon Sep  1 2008 Akira TAGOH <tagoh@redhat.com> - 2.0.18-5
- Update patch to fix a fuzz issue.

* Fri Jul  4 2008 Akira TAGOH <tagoh@redhat.com> - 2.0.18-4
- Add nkf, kakasi and mecab to BR. (#449186)

* Thu Mar 13 2008 Akira TAGOH <tagoh@redhat.com> - 2.0.18-1
- New upstream release.

* Tue Feb 12 2008 Akira TAGOH <tagoh@redhat.com> - 2.0.17-7
- Rebuild for gcc-4.3.

* Thu Nov 29 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-6
- Correct a timestamp for nmz-config.
- Add "replase" to %%config.
- Move %%post and %%postun to -libs.

* Sun Nov 25 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-5
- Move namazu.cgi from /var/www/cgi-bin to /usr/lib/namazu/ (#383521)

* Wed Nov 21 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-4
- Set the own script to __perl_requires.

* Tue Nov 20 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-3
- Get rid of -L@libdir@ from nmz-config because it's a standard library
  directory. (#342641)

* Mon Nov 19 2007 Akira TAGOH <tagoh@redhat.com>
- Clean up spec file. (#383521)
- Separate the shared library to namazu-libs package.

* Thu Aug 23 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-2
- Rebuild

* Fri Aug 10 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.17-1
- New upstream release.
- Update License tag.

* Wed Jan 17 2007 Akira TAGOH <tagoh@redhat.com> - 2.0.16-2
- remove .la (#222796)

* Thu Sep 14 2006 Akira TAGOH <tagoh@redhat.com> - 2.0.16-1
- New upstream release.
- namazu-newgettext3.patch: updated.

* Wed Mar  8 2006 Akira TAGOH <tagoh@redhat.com> - 2.0.15-3
- filter-requires-namazu.sh: updated to fix much more self-dependencies. (#184149)

* Thu Mar  2 2006 Akira TAGOH <tagoh@redhat.com> - 2.0.15-2
- New upstream release.
- namazu-2.0.13-de.patch: removed.
- namazu-fixinutf8.patch: updated.

* Mon May 30 2005 Akira TAGOH <tagoh@redhat.com> - 2.0.14-3
- rebuilt to import into Extras.

* Thu Mar 17 2005 Akira TAGOH <tagoh@redhat.com> - 2.0.14-2
- rebuilt

* Fri Dec 17 2004 Akira TAGOH <tagoh@redhat.com> - 2.0.14-1
- Security fix release.
  http://namazu.org/security.html

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Apr 16 2004 Akira TAGOH <tagoh@redhat.com> 2.0.13-2
- namazu-2.0.13-de.patch: applied to fix German templates.

* Fri Apr 16 2004 Akira TAGOH <tagoh@redhat.com> 2.0.13-1
- New upstream release.
- namazu-2.0.13-linguas.patch: updated.
- namazu-2.0.13-fixinutf8.patch: updated.
- namazu-2.0.12-de.diff: removed.

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jan 23 2003 Akira TAGOH <tagoh@redhat.com> 2.0.12-5
- namazu-2.0.12-fixinutf8.patch: applied to fix 'Malformed UTF-8 character'. (#80113)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Jan 08 2003 Akira TAGOH <tagoh@redhat.com> 2.0.12-3
- don't use rpms internal dep generator (#80966)

* Fri Nov 29 2002 Tim Powers <timp@redhat.com> 2.0.12-2
- remove second %%prep statement that rpm is choking on 

* Mon Nov 11 2002 Akira TAGOH <tagoh@redhat.com> 2.0.12-1
- New upstream release.
- namazu-2.0.12-de.diff: applied to add german templetes.
- remove duplicated files. but it was marked as unpackaged files.

* Thu Jul 18 2002 Akira TAGOH <tagoh@redhat.com> 2.0.10-8
- add the owned directory.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 30 2002 Chip Turner <cturner@redhat.com>
- add dependency filter for bogus perl dependencies

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Mar 25 2002 Akira TAGOH <tagoh@redhat.com> 2.0.10-4
- Don't ship ja.po for sjis.
- Striped our hostname from mknmzrc.
- Fix directories permission on /var/lib/namazu.

* Wed Feb 27 2002 Akira TAGOH <tagoh@redhat.com> 2.0.10-3
- Build against new environment.

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Dec 27 2001 Akira TAGOH <tagoh@redhat.com> 2.0.10-1
- New upstream release.
  includes a security fix by upstream.

* Fri Nov 30 2001 Akira TAGOH <tagoh@redhat.com> 2.0.9-1
- New upstream release.

* Tue Nov 27 2001 Akira TAGOH <tagoh@redhat.com> 2.0.8-1
- New upstream release.

* Tue Oct 30 2001 Akira TAGOH <tagoh@redhat.com> 2.0.7-2
- Fixed autoconf issue.

* Wed Oct 17 2001 Akira TAGOH <tagoh@redhat.com> 2.0.7-1
- Rebuild for Red Hat Linux.

* Fri Sep 14 2001 Ryuji Abe <rug@namazu.org> 2.0.7-1
- fix newgettext patch again.

* Tue Sep 11 2001 Ryuji Abe <rug@namazu.org> 2.0.6-2
- fix newgettext patch.

* Mon Aug 13 2001 Ryuji Abe <rug@namazu.org> 2.0.6-1
- update to 2.0.6

* Thu Jul 26 2001 Ryuji Abe <rug@namazu.org>
- fix %%files

* Sat Jun 23 2001 Ryuji Abe <rug@namazu.org>
- fix summary and %%description

* Thu May 31 2001 Ryuji Abe <rug@namazu.org>
- fix %%files
- fix again cgi-bin location to /var/www/cgi-bin

* Mon May 28 2001 Ryuji Abe <rug@namazu.org>
- clean up spec file
- more macros
- provide cgi package
- fix cgi-bin location /home/httpd/cgi-bin to /var/www/namazu-cgi-bin

* Wed Mar 21 2001 Ryuji Abe <rug@namazu.org>
- Rebuilt for 7.1 beta
- more macros
- fix dependencies
- exclude unnecessary ja_JP.SJIS catalog.

* Thu Oct 26 2000 Ryuji Abe <rug@namazu.org>
- Requires perl-File-MMagic >= 1.09.
- Add BuildRequires.

* Tue Aug 22 2000 Ryuji Abe <rug@namazu.org>
- Fixed %%localstatedir /var to /var/lib.

* Tue Apr 25 2000 Ryuji Abe <rug@namazu.org>
- Ignore %%{prefix}/share/namazu/etc.

* Sun Feb 20 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Install namazu.cgi at /home/httpd/cgi-bin.
- Fixed typo.

* Sat Feb 19 2000 Satoru Takabayashi <satoru-t@is.aist-nara.ac.jp>
- Change URL.

* Tue Feb 15 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Delete package entries elisp and cgi.

* Wed Feb 02 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Adapted for namazu-current.
- Changed group Utilities/Text -> Applications/Text.

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- rpm-3.0.x adaptations.
- Added package entries elisp and cgi (currently comment out). 
  [Merged SAKA Toshihide's changes for Kondara MNU/Linux.]

* Mon Nov 08 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Changed includedir %%{prefix}/include/namazu.
- Bug fix at configure section.

* Thu Nov 04 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Added nmz-config in devel package.

* Wed Nov 03 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Use our definite macros, ver, rel, prefix, sysconfdir, and localstatedir.
- If configure not found, use autogen.sh.
- Optimized for SMP environment.
- Build devel package.

* Tue Oct 12 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Fixed correctly executables entry at %%files.
- Added missing /usr/share/locale entry at %%files.
 
* Thu Aug 26 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Requires perl >= 5.004.
- Delete Packager tag.
- Clean up at %%prep.
- Use CFLAGS="$RPM_OPT_FLAGS" at %%build.
- Use $RPM_BUILD_ROOT variables at %%install.
- Change configure option at %%build and %%files for new namazu directory structure.

* Sun May 23 1999 Taku Kudoh <taku@TAHOO.ORG>
- 
